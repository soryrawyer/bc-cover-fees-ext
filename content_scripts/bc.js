/**
 * cover the bandcamp fees for an item
 * 
 * steps:
 * - get cell that has the price
 * - calculate the new price
 * - replace price text with the new price
 */


// Revenue share that bandcamp takes
const MARKUP_MAP = {
    "digital": 0.15,
    "merch": 0.1,
}


// class details for buy items including merch, excluding discount span
const priceCellSelector = "li.buyItem span.base-text-color:not(.discount-percentage), #merch-item .buy .compound-button span.base-text-color:not(.discount-percentage)";

// class detail for merch items
const merchClassSelector = "order_package_link";

const numRegex = /[1-9][0-9]*(\.[0-9]+)?/g;

const getCurrencyUnit = (priceText) => priceText.replace(numRegex, "");

const newPrice = (price, type) => (price / (1 - MARKUP_MAP[type])).toFixed(2);

const getPrice = (priceEl) => parseFloat(priceEl.innerText.match(numRegex));

/**
 * insert a new Header between the "buy album" h4 and the "send as gift" h4
 * 
 * get the current price and currency unit
 * create a new h4 element
 * get the gift h4
 * insert adjacent node before the gift element
 */
function addBcFees() {
    if (document.querySelector("div.cost-plus-fees") !== null) {
        return;
    }

    // get all buy items
    let priceEls = document.querySelectorAll(priceCellSelector);

    for (let i = 0; i < priceEls.length; i++) {

        // get the buy button element and check if it has the merch class
        let button = priceEls[i].parentElement.previousElementSibling;
        let type = button.classList.contains(merchClassSelector) ? "merch" : "digital";

        // get price & calc new price for each buy item
        let price = getPrice(priceEls[i]);
        let pricePlusFees = newPrice(price, type);
        let currencyUnit = getCurrencyUnit(priceEls[i].innerText);

        // format new element
        let newEl = document.createElement("div");
        newEl.className = "cost-plus-fees buyItemExtra secondaryText";
        newEl.innerText = `To cover Bandcamp fees: ${currencyUnit}${pricePlusFees}`;

        // insert below buy link, before gift option
        priceEls[i].parentElement.parentElement.append(newEl);

    }

}


function removeBcFees() {
    let elements = document.querySelectorAll("div.cost-plus-fees");
    for (let el of elements) {
        el.remove();
    }
}


const update = (bcToggleValue) => {
    if (bcToggleValue === "on") {
        addBcFees();
    } else {
        removeBcFees();
    }
}


browser.storage.onChanged.addListener((changes, area) => {
    if (area === "local" && "bcToggle" in changes) {
        console.log(`calling update with: ${changes.bcToggle.newValue}`);
        update(changes.bcToggle.newValue);
    }
});
