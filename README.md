# bandcamp friday every day

This Firefox extension will add another element to the Bandcamp "album" page that shows the original price _plus_ the percentage that Bandcamp takes from digital purchases. The idea is to have something similar to the "I'd like to cover processing fees" checkbox that a lot of donation forms have.

## testing
I used [web-ext](https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext/), and it worked well enough. I had to create a fake temporary directory, similar to the commenters on [this web-ext issue](https://github.com/mozilla/web-ext/issues/1696).  
To test:  
`TMPDIR=tmp-dir web-ext run --no-reload`

To build the zip file:  
`TMPDIR=tmp-dir web-ext build`
