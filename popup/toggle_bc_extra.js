/**
 * JS for toggling changes to prices on the page
 * should be loaded by popup/toggle_bc_extra.html
 */


async function listenForClicks() {
    document.getElementsByClassName("switch")[0].addEventListener("click", (event) => {
        let checkbox = document.querySelector("input");
        browser.storage.local.set({ bcToggle: checkbox.checked ? "on" : "off" }).catch(reportScriptError);
    });
}

listenForClicks().catch(e => console.error(e));

/**
 * there was an error executing the script
 * display the popup error message and hide the normal html
 */
function reportScriptError(error) {
    document.querySelector("#popup-content").classList.add("hidden");
    document.querySelector("#error-content").classList.remove("hidden");
    document.querySelector("#error-content").innerText = error.message;
}
